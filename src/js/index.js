(function(service) {

  document.addEventListener("DOMContentLoaded", function(event) {
    service.init(
      function onConnect() {
        const pEl = document.createElement('p');
        pEl.appendChild(document.createTextNode('/ -> client connect'));
        document.querySelector('#header').appendChild(pEl);
      },
      function onDisconnect() {
          const pEl = document.createElement('p');
          pEl.appendChild(document.createTextNode('/ -> client disconnect'));
          document.querySelector('#footer').appendChild(pEl);
      }
    );
  });

}(window.socket_service));
