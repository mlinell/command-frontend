window.http_service = (function(){

  const domain = "http://localhost:8080";

  function command(body)  {
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", domain + "/command");
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify(body));
  }

  function query(callback)  {
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          callback(JSON.parse(this.responseText));
        }
    };
    xmlhttp.open("GET", domain + "/query");
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send();
  }

  return {
    command: command,
    query: query
  };

}());
