window.socket_service = (function(){

  const url = 'http://localhost:8080';
  let stompClient;

  function command(topic, payload, callback) {
    if(!stompClient){
      throw new Error('socket is not initialized');
    }
    const commandResponse = stompClient.subscribe('/command/'+topic+'/response', (message) => {
      commandResponse.unsubscribe();
      message.body = JSON.parse(message.body);
      callback(message);
    });
    stompClient.send('/command/'+topic+'/request', {}, JSON.stringify(payload));
  }

  function query(topic, payload, callback) {
    if(!stompClient){
      throw new Error('socket is not initialized');
    }
    const commandResponse = stompClient.subscribe('/query/'+topic+'/response', (message) => {
      commandResponse.unsubscribe();
      message.body = JSON.parse(message.body);
      callback(message);
    });
    stompClient.send('/query/'+topic+'/request', {}, JSON.stringify(payload));
  }

  function subscribe(topic, payload, callback) {
    if(!stompClient){
      throw new Error('socket is not initialized');
    }
    stompClient.subscribe('/subscribe/'+topic+'/response', (message) => {
      message.body = JSON.parse(message.body);
      callback(message);
    });
    stompClient.send('/subscribe/'+topic+'/request', {}, JSON.stringify(payload));
  }

  function init(onConnectCallback, onDisconnectCallback) {
    const socket = new SockJS(url);
    stompClient = Stomp.over(socket);
    stompClient.connect(
    {},
    function (frame) {
      console.log('Connected: ' + frame);
      onConnectCallback();
    },
    function (frame) {
      console.log('Disconnected: ' + frame);
      if (!stompClient) {
        stompClient.disconnect();
      }
      onDisconnectCallback();
    }
    );
  }

  return {
    init: init,
    command: command,
    query: query,
    subscribe: subscribe
  };

}());
