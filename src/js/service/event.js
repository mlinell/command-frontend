window.events = (function(service){

  const topic = 'events';

  function commandEvents() {
    const command = 'create-event';
    service.command(command,
      {
        type: document.querySelector('#type').value,
        name: document.querySelector('#name').value
      },
      (frame) => {
          console.log('callback', command, frame);
          const pElement = document.createElement('p');
          pElement.appendChild(document.createTextNode('/command response -> command: ' + command + ', status: ' + frame.body.status));
          document.querySelector('#result').appendChild(pElement);
      }
    );
  }

  function queryEvents(){
    service.query(topic, {}, (frame) => {
      const all = document.querySelectorAll('#result > p');
       if(all){
         all.forEach(item => item.remove())
       }
      frame.body.forEach(event => {
          const pElement = document.createElement('p');
          pElement.appendChild(document.createTextNode('/query/response topic: ' + topic + ' -> client type: ' + event.type + ' name: ' + event.name));
          document.querySelector('#result').appendChild(pElement);
      });
    });
  }

  return {
    commandEvents: commandEvents,
    queryEvents: queryEvents
  };

}(window.socket_service));
