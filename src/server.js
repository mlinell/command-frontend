const express = require('express');
const app = express();
const http = require('http').Server(app);
const compression = require('compression');
const path = require('path');

const port = process.env.PORT || 8081;

app.disable('etag');
app.use(compression());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.use('/lib', express.static(path.join(__dirname, '..', 'node_modules')));
app.use('/js', express.static(path.join(__dirname, '.', 'js')));
app.use('/css', express.static(path.join(__dirname, '.', 'css')));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

http.listen(port, function() {
    console.log(`server at http://localhost:${port}`);
});
